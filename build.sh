#!/bin/sh

CGO_ENABLED=0 go build -trimpath -v -tags 'netgo osusergo static_build' -ldflags=all='-s -w' -gcflags=all='-l=4'
