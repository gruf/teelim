# teelim

Reads from stdin, and writes directly to given output path. Will ensure that file size never exceeds given max. Uses the `fallocate` syscall to truncate file from the start, requires a filesystem that supports `FALLOC_FL_COLLAPSE_RANGE`.

An example use is as an alternative to logrotate. By passing your service output through `teelim` you can guarantee a file size that never exceeds given max, but always keeps the latest "max" number of bytes.

e.g. `./gotosocial --config-path ./config.yaml server start | teelim -d 10MiB -m 90MiB -p 0600 log.txt`

