module codeberg.org/gruf/teelim

go 1.19

require (
	codeberg.org/gruf/go-bytesize v1.0.3
	codeberg.org/gruf/go-fflag v0.0.0-20241221142722-9c8ed7b8fed6
	codeberg.org/gruf/go-fsys v0.0.0-20230508183124-d1809af32b35
	golang.org/x/sys v0.15.0
)

require (
	codeberg.org/gruf/go-byteutil v1.3.0 // indirect
	codeberg.org/gruf/go-split v1.2.0 // indirect
)
