package main

import (
	"errors"
	"strconv"
)

// octalVar provides octal integer
// parsing for runtime CLI flags.
type octalVar uint32

func (v *octalVar) Set(in string) error {
	u, err := strconv.ParseUint(in, 8, strconv.IntSize)
	if err != nil {
		return err
	}
	*v = octalVar(u)
	return nil
}

func (*octalVar) Kind() string {
	return "octal"
}

func (v *octalVar) String() string {
	return strconv.FormatUint(uint64(*v), 8)
}

// wrap_err wraps an error with extra given msg.
func wrap_err(err error, msg string) error {
	return errors.New(msg + ": " + err.Error())
}

// round_up returns 'i' rounded up to the nearest 'by'.
func round_up(i int64, by int64) int64 {
	if i%by == 0 {
		return i
	}
	return (i/by + 1) * by
}
