package main

import (
	"errors"
	"io"
	"runtime/debug"

	"codeberg.org/gruf/go-bytesize"
	"codeberg.org/gruf/go-fflag"
	"codeberg.org/gruf/go-fsys"
	"golang.org/x/sys/unix"
)

var (
	// std file descriptors.
	stdin  = fsys.Stdin()
	stdout = fsys.Stdout()
	stderr = fsys.Stderr()
)

func main() {
	var code int

	// Run main application
	if err := run(); err != nil {
		stderr.WriteString("FATAL: " + err.Error() + "\n")
		code = 1
	}

	// Exit with code
	unix.Exit(code)
}

func run() error {
	var (
		// Read buffer
		readBuf []byte

		// Runtime flags
		maxsz bytesize.Size
		bufsz bytesize.Size
		delta bytesize.Size
		perms uint32
	)

	// Declare runtime flags to global fflag.FlagSet
	fflag.SizeVar(&delta, "d", "delta", 0, "Size delta between truncates")
	fflag.SizeVar(&maxsz, "m", "maximum", 10*bytesize.MiB, "Output file maximum size")
	fflag.SizeVar(&bufsz, "r", "read-buffer", 16*bytesize.KiB, "Stdin read buffer size")
	fflag.Var((*octalVar)(&perms), "p", "permissions", "0644", "Output file permissions")
	fflag.Version(version())
	fflag.Help()

	// Parse runtime flags
	args, err := fflag.Parse()
	if err != nil {
		return err
	}

	switch {
	case len(args) == 0:
		return errors.New("no output path supplied")

	case len(args) > 1:
		return errors.New("multiple output paths supplied")

	case maxsz == 0:
		return errors.New("max size must be greater than zero")

	case bufsz < 64:
		// Enforce a min. buf size
		bufsz = 4 * bytesize.KiB
	}

	// Open output file at path
	fd, stat, err := fsys.OpenFile(
		args[0],
		unix.O_WRONLY|
			unix.O_CREAT|
			unix.O_APPEND,
		uint32(perms),
	)
	if err != nil {
		return wrap_err(err, "error opening file "+args[0])
	}

	// NOTE: we leave file descr close
	// on error up to Linux to handle.

	// Attempt a single fallocate to confirm supported
	if err := fd.Fallocate(unix.FALLOC_FL_COLLAPSE_RANGE, 0, 1); err == unix.ENOTSUP {
		return errors.New("system does not support fallocate(FALLOC_FL_COLLAPSE_RANGE)")
	}

	// Start file size
	size := stat.Size

	// Calculate maximum extended size
	extsz := int64(maxsz) + int64(delta)

	// Allocate read buffer of size
	readBuf = make([]byte, bufsz)

	for done := false; !done; {

		// Check if our internal
		// size est. extends beyond
		// max extended (max + delta).
		if size-extsz > 0 {

			// Double check file size
			stat, err := fd.Stat()
			if err != nil {
				return wrap_err(err, "error statting file "+args[0])
			}

			// Set updated size
			size = stat.Size

			if diff := size - int64(maxsz); diff > 0 {
				// Round diff up to nearest block size
				diff = round_up(diff, int64(stat.Blksize))

				// Output is too large, truncate the file from the start
				if err := fd.Fallocate(unix.FALLOC_FL_COLLAPSE_RANGE, 0, diff); err != nil {
					return wrap_err(err, "error truncating file "+args[0])
				}

				// Update to truncated
				size = int64(maxsz)
			}
		}

		// Read next chunk from stdin
		n, err := stdin.Read(readBuf)

		switch err {
		// Full read
		case nil:

		// Reached end
		case io.EOF:
			done = true

		// Unexpected
		default:
			return wrap_err(err, "error reading stdin")
		}

		// Write buffered bytes to output file
		if _, err := fd.Write(readBuf[:n]); err != nil {
			return wrap_err(err, "error writing to file "+args[0])
		}

		// Update est. size
		size += int64(n)
	}

	// Close output.
	_ = fd.Close()

	return nil
}

func version() string {
	var flags, commit, time string
	build, _ := debug.ReadBuildInfo()
	for i := 0; i < len(build.Settings); i++ {
		switch build.Settings[i].Key {
		case "-gcflags":
			flags += ` -gcflags="` + build.Settings[i].Value + `"`
		case "-ldflags":
			flags += ` -ldflags="` + build.Settings[i].Value + `"`
		case "-tags":
			flags += ` -tags="` + build.Settings[i].Value + `"`
		case "vcs.revision":
			commit = build.Settings[i].Value
			if len(commit) > 8 {
				commit = commit[:8]
			}
		case "vcs.time":
			time = build.Settings[i].Value
		}
	}
	return "commit=" + commit + "\n" +
		"build=" + build.GoVersion + flags + "\n" +
		"time=" + time
}
